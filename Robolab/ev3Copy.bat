echo off
cls

set password="demgeilem"
set roboip="169.254.183.115"

if %roboip% == "172.16.XXX.XXX" (
    echo "Erst IP anpassen (rechtsklick -> Bearbeiten)"
    pause
    exit
)
    
echo "Kopieren der Daten auf den Robot"
.\bin\pscp -pw %password% ./src/*.py robot@%roboip%:
echo "Ausf�hren der Skripte"
.\bin\putty.exe -pw %password% -ssh robot@%roboip% -m .\bin\command.txt -t
echo "Finished"
pause
exit
