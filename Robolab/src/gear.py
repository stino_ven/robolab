import ev3dev.ev3 as ev3

standardspeed = -70


def motor(output):
    motor = ev3.LargeMotor(output)
    motor.command = 'run-direct'
    motor.duty_cycle_sp = standardspeed

def motorStop(output):
    motor = ev3.LargeMotor(output)
    motor.stop()


