#!/usr/bin/env python

import ev3dev.ev3 as ev3
import array
import fcntl
import sys
import time
g = __import__("gear")
s = __import__("sense")


# from linux/input.h

KEY_UP = 103
KEY_DOWN = 108
KEY_LEFT = 105
KEY_RIGHT = 106
KEY_ENTER = 28
KEY_BACKSPACE = 14

KEY_MAX = 0x2ff

def EVIOCGKEY(length):
    return 2 << (14+8+8) | length << (8+8) | ord('E') << 8 | 0x18

# end of stuff from linux/input.h

BUF_LEN = (KEY_MAX + 7) / 8

def test_bit(bit, bytes):
    # bit in bytes is 1 when released and 0 when pressed
    return not bool(bytes[bit / 8] & (1 << (bit % 8)))

def main():
    print(s.getValue())
    while(s.getValue() != 5):
        print(s.getValue())
        if(s.getValue() == 1):
                g.motor('outC')
        elif(s.getValue() == 6):
                g.motor('outB')
        else:
            g.motor('outB')
            g.motor('outC')


if __name__ == "__main__":
    main()
